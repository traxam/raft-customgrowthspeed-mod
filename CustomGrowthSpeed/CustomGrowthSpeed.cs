﻿using System;
using System.Globalization;
using System.Reflection;
using HarmonyLib;
using Steamworks;
using UnityEngine;

namespace CustomGrowthSpeed
{
    public class CustomGrowthSpeedMod : Mod
    {
        private const string PlayerPrefsGrowthSpeed = "traxam.CustomGrowthSpeed.growthSpeed";
        private const string HarmonyId = "traxam.CustomGrowthSpeed";
        private static float _currentGrowthSpeed = 1;
        private Harmony _harmony;

        public void Start()
        {
            _harmony = new Harmony(HarmonyId);
            _harmony.PatchAll(Assembly.GetExecutingAssembly());

            Info("Mod was loaded successfully!");
            if (PlayerPrefs.HasKey(PlayerPrefsGrowthSpeed))
            {
                _currentGrowthSpeed = PlayerPrefs.GetFloat(PlayerPrefsGrowthSpeed);
                FollowUpLog("The current growth speed is <color=green>" + _currentGrowthSpeed + "</color>.");
            }

            FollowUpLog(
                "Type <color=green>growthspeed <speed></color> to change the growth speed, i.e. <color=green>growthspeed 2.5</color> (1 is the default growth speed).");
        }

        [ConsoleCommand("growthspeed", "Change the growth speed")]
        // ReSharper disable once UnusedMember.Local
        private static void HandleGrowthSpeedCommand(string[] arguments)
        {
            if (arguments.Length < 1)
            {
                Info("The current growth speed is <color=green>" + _currentGrowthSpeed + "</color>.");
                FollowUpLog(
                    "Type <color=green>growthspeed <speed></color> to change the growth speed, i.e. <color=green>growthspeed 2.5</color> (1 is the default growth speed).");
            }
            else
            {
                try
                {
                    var value = float.Parse(arguments[0], CultureInfo.InvariantCulture);
                    if (value <= 0)
                    {
                        Error("The provided growth speed (<color=green>" + arguments[0] +
                              "</color>) is not a positive value.");
                        FollowUpLog(
                            "Please provide a positive decimal growth speed value, i.e. <color=green>growthspeed 2.5</color> (1 is the default growth speed).");
                    }
                    else
                    {
                        _currentGrowthSpeed = value;
                        PlayerPrefs.SetFloat(PlayerPrefsGrowthSpeed, value);
                        PlayerPrefs.Save();
                        Info("Growth speed was set to <color=green>" + value +
                             "</color>. Type <color=green>growthspeed 1</color> to reset it.");
                    }
                }
                catch (FormatException)
                {
                    Error("<color=green>" + arguments[0] +
                          "</color> is not a valid value. Please provide a positive decimal growth speed value, i.e. <color=green>growthspeed 2.5</color> (1 is the default growth speed).");
                }
            }
        }

        public void OnModUnload()
        {
            _harmony.UnpatchAll(HarmonyId);
            _currentGrowthSpeed = 1;
            Info("Growth speed was reset.");
        }

        private static void Info(string message)
        {
            Debug.Log("<color=#3498db>[info]</color>\t<b>traxam's CustomGrowthSpeed:</b> " + message);
        }

        private static void FollowUpLog(string message)
        {
            Debug.Log("\t" + message);
        }

        private static void Error(string message)
        {
            Debug.LogError("<color=#e74c3c>[error]</color>\t<b>traxam's CustomGrowthSpeed:</b> " + message);
        }

        [HarmonyPatch(typeof(Plant))]
        [HarmonyPatch("Grow")]
        // This method is copied from the original Raft source code and was slightly modified to allow damage value
        // modification. To change as few things as possible, we are not considering ReSharper's code warnings.
        // ReSharper disable All
        private class PlantGrowPatch
        {
            private static bool Prefix(Semih_Network network, Network_Player localPlayer,
                Plant __instance,
                ref bool ___fullyGrown,
                ref float ___growTimer,
                ref float ___growTimeSec)
            {
                if (___fullyGrown)
                {
                    return false;
                }

                float d = Mathf.Clamp(___growTimer / ___growTimeSec, 0f, 1f);
                Vector3 target = __instance.minScale + (__instance.maxScale - __instance.minScale) * d;
                __instance.transform.localScale =
                    Vector3.MoveTowards(__instance.transform.localScale, target, Time.deltaTime * 20f);
                ___growTimer += Time.deltaTime * CustomGrowthSpeedMod._currentGrowthSpeed;
                if (___growTimer >= ___growTimeSec)
                {
                    if (Semih_Network.IsHost)
                    {
                        Message_Plant_Complete message = new Message_Plant_Complete(Messages.PlantManager_PlantComplete,
                            localPlayer.PlantManager, __instance.cropplot, __instance);
                        network.RPC(message, Target.Other, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                        __instance.Complete();
                        return false;
                    }

                    ___fullyGrown = true;
                }

                return false;
            }
        }
        // ReSharper restore All
    }
}