![banner image](./CustomGrowthSpeed/banner.png)

# Custom Growth Speed
*([view on RaftModding](https://www.raftmodding.com/mods/customgrowthspeed))*

This mod lets you customize how fast trees, crops and other plants are growing.

## Changing the growth speed
Open the console by pressing the `[F10]` key and type the following command to change the plant growth speed. 

**Command: `growthspeed <speed>`**

**Example:** Typing `growthspeed 2` will double the plants' growing speed.

## Demo
The GIF below shows a tree growing with a growth speed of `30`.

![demo gif with a fast growing tree](https://traxam.s-ul.eu/4KxNQ9kP.gif)

## Support
If you have any issues with or ideas for this mod, please message me (@traxam#7012) in the modding section of the [official Raft Discord-server](https://discord.gg/raft).

## Credits
- developed by [traxam](https://trax.am)
- [icon image](https://www.flaticon.com/free-icon/plant_459649) by [Freepik](https://www.freepik.com/), licensed [Creative Commons BY 3.0](http://creativecommons.org/licenses/by/3.0/)
- Thanks to `Rikashade#5975` for the idea